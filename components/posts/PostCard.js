import React from "react";
import Image from "next/image";
import Link from "next/link";
import styles from "../../styles/PostCard.module.scss";

const myLoader = ({ src, width, quality }) => {
  return `https://example.com/${src}?w=${width}&q=${quality || 75}`;
};

export default function PostCard({ post }) {
  return (
    <div>
      <Link href={`/posts/${post.slug}`}>
        <div className={styles.card__image_container}>
          <img
            // loader={myLoader}
            src={post.cover_image}
            alt={post.title}
          />
        </div>
      </Link>
      <h4 className={styles.card__title}>{post.title}</h4>
      <span className={styles.card__date}>{post.date}</span>
      <p className={styles.card__excerpt}>{post.excerpt}</p>
      <Link href={`/posts/${post.slug}`} className={styles.card__link}>
        Read More
      </Link>
    </div>
  );
}
