import React from "react";
import Link from "next/link";

export default function Menu() {
  return (
    <div>
      <nav className="container">
        <Link href={"/"}>
          <h2>Anthony Thagichu</h2>
        </Link>
        <ul>
          <li>
            <Link href="/">Home</Link>
          </li>
          <li>
            <Link href="/">About</Link>
          </li>
          <li>
            <Link href="/">Projects</Link>
          </li>
          <li>
            <Link href="/">Blog</Link>
          </li>
        </ul>
      </nav>
    </div>
  );
}
