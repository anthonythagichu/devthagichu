---
title: "The all-in-one app for notes, tasks, project management (not only) for Developers
"
date: "Jun 4 2022"
excerpt: "Hi guys! I hope that despite the current situation with coronavirus and COVID-19 you’re doing well 😷. In this short text..."
cover_image: "/images/posts/post-two.jpg"
---

# Post Two

Hi guys! I hope that despite the current situation with coronavirus and COVID-19 you’re doing well 😷. In this short text, I would like to share with you a great app I started using to keep my notes, ideas, and planning and managing my IT projects.

However, I would like to point out that the application can be used for everything and in every field. So if you’re working remotely now or you want to better plan your day, write your ideas, inspirations, manage tasks, create to-do lists, etc. in a digital way — I expect you can like it as much as I do.
