---
title: "WorldRemit and Safaricom partner to enable instant cross-border money transfers for MPESA users"
date: "Jun 6 2022"
excerpt: "Over the years, money remittances have played an important role in Kenya’s economy with local remittances reaching a record value of $1.95 billion (196 billion KSH) according to Kenya’s Central Bank, making it one the country’s top earners..."
cover_image: "/images/posts/mpesa.jpg"
---

Over the years, money remittances have played an important role in Kenya’s economy with local remittances reaching a record value of $1.95 billion (196 billion KSH) according to Kenya’s Central Bank, making it one the country’s top earners.

With around three million Kenyans currently living abroad, with large communities in North America, Europe and Australia, they’ve remained the biggest mobile-to-mobile senders of remittances to their family and friends in Kenya using WorldRemit.

That’s why WorldRemit, one of the world’s leading digital money transfer services, announced today that it is deepening its partnership with Kenya’s Safaricom to enable money transfers to 28.6 million M-PESA accounts directly from their phones.
