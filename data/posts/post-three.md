---
title: "React Fiber, Virtual DOM and Shadow DOM - The Ultimate Front-End Interview Questions Guide"
date: "Jun 5 2022"
excerpt: "Welcome to the Ultimate Front-End Interview Guide. In this series of posts you will find the most demanded topics in Front-End interviews for you to do well in all of them. Even if you are not preparing..."
cover_image: "/images/posts/post-three.jpg"
---

Welcome to the Ultimate Front-End Interview Guide. In this series of posts you will find the most demanded topics in Front-End interviews for you to do well in all of them. Even if you are not preparing for an interview right now, this series of posts will keep you up to date and always prepared to explain complex topics related to the most derivative technologies of interface construction.

## Are you feeling DOMinated?

If we ask someone who has minimal knowledge of the concept of the web, this person will know what HTML is and its structure. Between so many existing tags and the tree that builds what we see on the screen, we can see and understand the DOM concept.

According to the definition of the mdn web docs:

The Document Object Model (DOM) is a programming interface for web documents. It represents the page so that programs can change the document structure, style, and content. The DOM represents the document the nodes and objects; that way, programming languages ​​can interact with the page.

## html dom

In a nutshell, the DOM is a representation of the tree of elements parsed by our browser, whatever it may be. Any modification that changes the style, content or structure of our page represents a change to the DOM.

And where do the other nomenclatures come from? Now let's see the definition of each of them.
