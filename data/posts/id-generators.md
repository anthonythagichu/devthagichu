---
title: "Unique ID generators for Javascript"
date: "Jun 7 2022"
excerpt: "A list of common javascript unique id generators..."
cover_image: "/images/posts/id.jpg"
---

1. #### UUID

https://www.npmjs.com/package/uuid

```
npm install --save uuid

```

```
import { v4 as uuidv4 } from 'uuid';

uuidv4(); // "9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d"

```

2. #### NANOID

https://www.npmjs.com/package/nanoid

```
npm install --save nanoid

```

```
import { nanoid } from 'nanoid'

nanoid() // "V1StGXR8_Z5jdHi6B-myT"

```

3. #### CUID

https://www.npmjs.com/package/cuid

```
npm install --save cuid

```

```
import cuid from 'cuid';

cuid(); // cjld2cjxh0000qzrmn831i7rn

```

4. #### SHORTID

https://www.npmjs.com/package/shortid

```
npm install --save shortid

```

```
import shortid from 'shortid';

shortid.generate(); // PPBqWA9
```
