---
title: "Breakpoints and media queries in SCSS"
date: "Jun 3 2022"
excerpt: "I always build my websites in a mobile-first approach, so I don’t need to define the smallest screen size (xs – extra small) and I write my SCSS code first for the smallest devices and next for the largest..."
cover_image: "/images/posts/post-one.jpg"
---

Hi! I would like to show you breakpoints mixins I used in my SCSS code.

Screen size definitions
First we need to define our breakpoints – width of the screen when other styles should be applied. We can do something like this:

As you can see, in this example I use naming based on Twitter Bootstrap grid.

Mixins
Now it’s time to create the most important element – mixins:

I always build my websites in a mobile-first approach, so I don’t need to define the smallest screen size (xs – extra small) and I write my SCSS code first for the smallest devices and next for the largest. Sometimes we also need to define some styles beyond the rigidly defined breakpoints. Let’s add one more mixin – I called it “rwd”:

As a parameter $screen we can put any screen size.
