import Head from "next/head";
import fs from "fs";
import path from "path";
import * as matter from "gray-matter";
import styles from "../styles/Home.module.scss";
import PostCard from "../components/posts/PostCard";

export default function Home({ posts }) {
  return (
    <div>
      <Head>
        <title>Anthony Thagichu</title>
        <meta name="description" content="Anthony Thagichu Blog" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      {/* <div class="hero">
        <div class="left"></div>
        <div class="right">
          <div class="box">
            <span>hello</span>
            <h1>June 2nd, 2022: What did you learn this week?</h1>
          </div>
        </div>
      </div> */}
      <div class="container">
        <div class="blog__section">
          <div class="blog">
            {posts?.map((post) => {
              return <PostCard post={post} key={post.slug} />;
            })}
          </div>
          <div class="">
            <h3>hey</h3>
          </div>
        </div>
      </div>
    </div>
  );
}

export async function getStaticProps() {
  const post_path = path.join("data", "posts");
  const post_files = fs.readdirSync(post_path);

  const posts = post_files.map((post_file) => {
    let slug = post_file.replace(".md", "");
    let markdownWithMeta = fs.readFileSync(
      path.join(post_path, post_file),
      "utf-8"
    );
    const { data: frontMatter } = matter(markdownWithMeta);

    return {
      slug,
      ...frontMatter,
    };
  });

  console.log(posts);

  return {
    props: {
      posts,
    },
  };
}
