import Footer from "../components/Layout/Footer";
import Menu from "../components/Layout/Menu";
import "../styles/globals.scss";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Menu />

      <Component {...pageProps} />

      <Footer />
    </>
  );
}

export default MyApp;
