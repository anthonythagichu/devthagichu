import React from "react";
import Head from "next/head";
import Image from "next/image";
import { serialize } from "next-mdx-remote/serialize";
import { MDXRemote } from "next-mdx-remote";
import * as matter from "gray-matter";
import fs from "fs";
import path from "path";
// const myLoader = ({ src, width, quality }) => {
//   return "/images/posts/post-one.jpg";
// };
const components = {
  img: (props) => <img width={"60%"} className="h1" {...props} />,
  h1: (props) => <h1 className="h1" {...props} />,
  h2: (props) => <h2 className="h2" {...props} />,
  h3: (props) => <h3 className="h3" {...props} />,
  h4: (props) => <h4 className="h4" {...props} />,
  h5: (props) => <h5 className="h5" {...props} />,
  h6: (props) => <h6 className="h6" {...props} />,
  p: (props) => <p className="body__text" {...props} />,
  code: (props) => (
    <pre
      style={{
        padding: "16px",
        overflow: "auto",
        fontSize: "85%",
        lineHeight: 1.45,
        backgroundColor: "#f6f8fa",
        borderRadius: "3px",
      }}
      className="code"
      {...props}
    />
  ),
};

export default function PostPage({ post }) {
  return (
    <div>
      <Head>
        <title>Anthony Thagichu | {post.title}</title>
        <meta name="description" content={post.excerpt} />
      </Head>
      <div>
        <div className="container">
          <h1>{post.title}</h1>
          <p>{post.date}</p>
        </div>
        <img
          className="post_img"
          //   loader={myLoader}
          src={post.cover_image}
          alt={post.title}
        />
        <div className="container">
          <MDXRemote {...post.content} components={components} />
        </div>
      </div>
    </div>
  );
}

export async function getStaticPaths() {
  const post_path = path.join("data", "posts");
  const post_files = fs.readdirSync(post_path);
  const paths = post_files.map((file) => ({
    params: {
      slug: file.replace(".md", ""),
    },
  }));

  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params: { slug } }) {
  const post_path = path.join("data", "posts");
  const markdownWithMeta = fs.readFileSync(
    path.join(post_path, `${slug}.md`),
    "utf-8"
  );
  const { data: frontMatter, content } = matter(markdownWithMeta);

  const mdxSource = await serialize(content);
  return {
    props: {
      post: {
        slug,
        ...frontMatter,
        content: mdxSource,
      },
    },
  };
}
